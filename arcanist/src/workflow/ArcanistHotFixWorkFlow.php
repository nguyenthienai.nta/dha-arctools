<?php

/**
 * Displays user's Git branches or Mercurial bookmarks.
 *
 * @concrete-extensible
 */
class ArcanistHotFixWorkFlow extends ArcanistWorkflow {

  private $branches;

  public function getWorkflowName() {
    return 'hotfix';
  }

  public function getCommandSynopses() {
    return phutil_console_format(<<<EOTEXT
      **hotfix** start __version__
      **hotfix** finish __version__
      **hotfix** delete __version__
EOTEXT
    );
  }

  public function getCommandHelp() {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git branch' or 'hg bookmark'.

          With start [__branch name__], The version argument hereby marks the new
          hotfix release name. Optionally you can specify a basename to start from.

          By finishing a hotfix it gets merged back into develop and master.
          Additionally the master merge is tagged with the hotfix version.
EOTEXT
    );
  }

  public function requiresConduit() {
    return true;
  }

  public function requiresRepositoryAPI() {
    return true;
  }

  public function requiresAuthentication() {
    return !$this->getArgument('branch');
  }

  public function getArguments() {
    return array(
      'list' => array(
        'help' => pht('List hotfix branch'),
      ),
      'f' => array(
        'help' => pht('Force deletion'),
      ),
      '*' => 'branch',
    );
  }

  public function getSupportedRevisionControlSystems() {
    return array('git', 'hg');
  }

  public function run() {
    $repository_api = $this->getRepositoryAPI();

    $filter = $this->getArgument('list');

    if ($filter) {
      return $this->filterBranch($repository_api);
    }

    $names = $this->getArgument('branch');
    if ($names) {
      if (count($names) > 2) {
        throw new ArcanistUsageException(pht('Specify only one branch.'));
      }
      if($names[0] == 'start') {
        return $this->startBranch($names, $repository_api);
      }
      if($names[0] == 'finish') {
        return $this->finishBranch($names, $repository_api);
      }
      if($names[0] == 'delete') {
        return $this->deleteBranch($names, $repository_api);
      }
    }
    return 0;
  }

  private function checkoutBranch(array $names) {
    $api = $this->getRepositoryAPI();

    if ($api instanceof ArcanistMercurialAPI) {
      $command = 'update %s';
    } else {
      $command = 'checkout %s';
    }

    $err = 1;

    $name = $names[0];
    if (isset($names[1])) {
      $start = $names[1];
    } else {
      $start = $this->getConfigFromAnySource('dha.feature.start.default');
    }

    $branches = $api->getAllBranches();
    if (in_array($name, ipull($branches, 'name'))) {
      list($err, $stdout, $stderr) = $api->execManualLocal($command, $name);
    }

    if ($err) {
      $match = null;
      if (preg_match('/^D(\d+)$/', $name, $match)) {
        try {
          $diff = $this->getConduit()->callMethodSynchronous(
            'differential.querydiffs',
            array(
              'revisionIDs' => array($match[1]),
            ));
          $diff = head($diff);

          if ($diff['branch'] != '') {
            $name = $diff['branch'];
            list($err, $stdout, $stderr) = $api->execManualLocal(
              $command,
              $name);
          }
        } catch (ConduitClientException $ex) {}
      }
    }

    if ($err) {
      if ($api instanceof ArcanistMercurialAPI) {
        $rev = '';
        if ($start) {
          $rev = csprintf('-r %s', $start);
        }

        $exec = $api->execManualLocal('bookmark %C %s', $rev, $name);

        if (!$exec[0] && $start) {
          $api->execxLocal('update %s', $name);
        }
      } else {
        $startarg = $start ? csprintf('%s', $start) : '';
        $exec = $api->execManualLocal(
          'checkout --track -b %s %C',
          $name,
          $startarg);
      }

      list($err, $stdout, $stderr) = $exec;
    }

    echo $stdout;
    fprintf(STDERR, '%s', $stderr);
    return $err;
  }

  private function startBranch(array $names, $repository_api)
  {
    $master = false;
    $branches = $repository_api->getAllBranches();
    foreach ($branches as $branch) {
      if($branch['name'] == 'master') {
        $master = $branch['name'];
        break;
      }
    }
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }
    if($master) {
      $exec = $repository_api->execManualLocal(
        'checkout --track -b %s %C',
        'hotfix/' . $names[1],
        $master);

      list($err, $stdout, $stderr) = $exec;
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else {
      throw new ArcanistUsageException(pht('Do not find branch master.'));
    }
  }

  private function finishBranch(array $names, $repository_api) {
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }

    exec('git rev-parse --abbrev-ref HEAD', $output);
    $branches = $repository_api->getAllBranches();
    $develop = false;

    exec('git status', $statuses);
    if(in_array('nothing to commit, working tree clean',$statuses)) {
      $treeClean = true;
    } else {
      $treeClean = false;
    }

    foreach ($branches as $branch) {
      if($branch['name'] == 'develop' || $branch['name'] == 'development') {
        $develop = $branch['name'];
        break;
      }
    }
    if($develop) {
      $hotfixBranch = $names[1];

      $exec = $repository_api->execManualLocal(
        $treeClean ? 'checkout %s' : 'stash -u && git checkout %s',
        'master');
      list($err, $stdout, $stderr) = $exec;
      if($err === 0) {
        $exec = $repository_api->execManualLocal(
          'merge --no-ff hotfix/%C && git tag %s', $hotfixBranch, 'hotfix/v'.$hotfixBranch);
        list($err, $stdout, $stderr) = $exec;
        echo $stdout;
        fprintf(STDERR, '%s', $stderr);
      }
      if($err === 0) {
        $exec = $repository_api->execManualLocal(
          'checkout %s', $develop);
        list($err, $stdout, $stderr) = $exec;
        fprintf(STDERR, '%s', $stderr);
      }
      if($err === 0) {
        $exec = $repository_api->execManualLocal(
          'merge --no-ff hotfix/%s', $hotfixBranch);
        list($err, $stdout, $stderr) = $exec;
        echo $stdout;
        fprintf(STDERR, '%s', $stderr);
      }
      if($treeClean === false && $err === 0) {
        $exec = $repository_api->execManualLocal(
          'stash pop');
      }
      if($err === 0) {
        $exec = $repository_api->execManualLocal(
          'branch -d hotfix/%s', $hotfixBranch);
        list($err, $stdout, $stderr) = $exec;
      }

      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else {
      throw new ArcanistUsageException(pht('Do not find branch develop or development.'));
    }

  }

  private function deleteBranch(array $names, $repository_api) {
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }

    $forceDelete = $this->getArgument('f');
    $branchToDelete = 'hotfix/' . $names[1];
    list($err) = $repository_api->execManualLocal(
      'rev-parse --verify %s',
      $branchToDelete);

    if ($err) {
      throw new ArcanistUsageException(
        pht("Branch '%s' does not exist.", $branchToDelete));
    }

    exec('git status', $status);
    if (in_array('nothing to commit, working tree clean', $status)) {
      $exec = $repository_api->execManualLocal(
        'checkout %s', 'develop');
      list($err, $stdout, $stderr) = $exec;
      fprintf(STDERR, '%s', $stderr);

      if ($forceDelete) {
        $exec = $repository_api->execManualLocal(
          'branch -D %s', $branchToDelete);
        list($err, $stdout, $stderr) = $exec;
      } else {
        $exec = $repository_api->execManualLocal(
          'branch -d %s', $branchToDelete);
        list($err, $stdout, $stderr) = $exec;

        if ($err)
          $stderr = phutil_console_format(
            "Fatal: Hotfix branch '{$branchToDelete}' has been not been merged in branch 'master' and/or branch 'develop'. Use --f to force the deletion.\n");
      }
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else
      echo phutil_console_format(
        "Fatal: Working tree contains unstaged changes. Aborting.\n");
  }

  private function loadCommitInfo(array $branches) {
    $repository_api = $this->getRepositoryAPI();

    $branches = ipull($branches, null, 'name');

    if ($repository_api instanceof ArcanistMercurialAPI) {
      $futures = array();
      foreach ($branches as $branch) {
        $futures[$branch['name']] = $repository_api->execFutureLocal(
          'log -l 1 --template %s -r %s',
          "{node}\1{date|hgdate}\1{p1node}\1{desc|firstline}\1{desc}",
          hgsprintf('%s', $branch['name']));
      }

      $futures = id(new FutureIterator($futures))
        ->limit(16);
      foreach ($futures as $name => $future) {
        list($info) = $future->resolvex();

        $fields = explode("\1", trim($info), 5);
        list($hash, $epoch, $tree, $desc, $text) = $fields;

        $branches[$name] += array(
          'hash' => $hash,
          'desc' => $desc,
          'tree' => $tree,
          'epoch' => (int)$epoch,
          'text' => $text,
        );
      }
    }

    foreach ($branches as $name => $branch) {
      $text = $branch['text'];

      try {
        $message = ArcanistDifferentialCommitMessage::newFromRawCorpus($text);
        $id = $message->getRevisionID();

        $branch['revisionID'] = $id;
      } catch (ArcanistUsageException $ex) {
        // In case of invalid commit message which fails the parsing,
        // do nothing.
        $branch['revisionID'] = null;
      }

      $branches[$name] = $branch;
    }

    return $branches;
  }

  private function loadRevisions(array $branches) {
    $ids = array();
    $hashes = array();

    foreach ($branches as $branch) {
      if ($branch['revisionID']) {
        $ids[] = $branch['revisionID'];
      }
      $hashes[] = array('gtcm', $branch['hash']);
      $hashes[] = array('gttr', $branch['tree']);
    }

    $calls = array();

    if ($ids) {
      $calls[] = $this->getConduit()->callMethod(
        'differential.query',
        array(
          'ids' => $ids,
        ));
    }

    if ($hashes) {
      $calls[] = $this->getConduit()->callMethod(
        'differential.query',
        array(
          'commitHashes' => $hashes,
        ));
    }

    $results = array();
    foreach (new FutureIterator($calls) as $call) {
      $results[] = $call->resolve();
    }

    return array_mergev($results);
  }

  private function printBranches(array $branches, array $revisions) {
    $revisions = ipull($revisions, null, 'id');

    static $color_map = array(
      'Closed'          => 'cyan',
      'Needs Review'    => 'magenta',
      'Needs Revision'  => 'red',
      'Accepted'        => 'green',
      'No Revision'     => 'blue',
      'Abandoned'       => 'default',
    );

    static $ssort_map = array(
      'Closed'          => 1,
      'No Revision'     => 2,
      'Needs Review'    => 3,
      'Needs Revision'  => 4,
      'Accepted'        => 5,
    );

    $out = array();
    foreach ($branches as $branch) {
      $revision = idx($revisions, idx($branch, 'revisionID'));

      // If we haven't identified a revision by ID, try to identify it by hash.
      if (!$revision) {
        foreach ($revisions as $rev) {
          $hashes = idx($rev, 'hashes', array());
          foreach ($hashes as $hash) {
            if (($hash[0] == 'gtcm' && $hash[1] == $branch['hash']) ||
              ($hash[0] == 'gttr' && $hash[1] == $branch['tree'])) {
              $revision = $rev;
              break;
            }
          }
        }
      }

      if ($revision) {
        $desc = 'D'.$revision['id'].': '.$revision['title'];
        $status = $revision['statusName'];
      } else {
        $desc = $branch['desc'];
        $status = pht('No Revision');
      }

      if (!$this->getArgument('view-all') && !$branch['current']) {
        if ($status == 'Closed' || $status == 'Abandoned') {
          continue;
        }
      }

      $epoch = $branch['epoch'];

      $color = idx($color_map, $status, 'default');
      $ssort = sprintf('%d%012d', idx($ssort_map, $status, 0), $epoch);

      $out[] = array(
        'name'      => $branch['name'],
        'current'   => $branch['current'],
        'status'    => $status,
        'desc'      => $desc,
        'revision'  => $revision ? $revision['id'] : null,
        'color'     => $color,
        'esort'     => $epoch,
        'epoch'     => $epoch,
        'ssort'     => $ssort,
      );
    }

    if (!$out) {
      // All of the revisions are closed or abandoned.
      return;
    }

    $len_name = max(array_map('strlen', ipull($out, 'name'))) + 2;
    $len_status = max(array_map('strlen', ipull($out, 'status'))) + 2;

    if ($this->getArgument('by-status')) {
      $out = isort($out, 'ssort');
    } else {
      $out = isort($out, 'esort');
    }
    if ($this->getArgument('output') == 'json') {
      foreach ($out as &$feature) {
        unset($feature['color'], $feature['ssort'], $feature['esort']);
      }
      echo json_encode(ipull($out, null, 'name'))."\n";
    } else {
      $table = id(new PhutilConsoleTable())
        ->setShowHeader(false)
        ->addColumn('current', array('title' => ''))
        ->addColumn('name',    array('title' => pht('Name')))
        ->addColumn('status',  array('title' => pht('Status')))
        ->addColumn('descr',   array('title' => pht('Description')));

      foreach ($out as $line) {
        $table->addRow(array(
          'current' => $line['current'] ? '*' : '',
          'name'    => tsprintf('**%s**', $line['name']),
          'status'  => tsprintf(
            "<fg:{$line['color']}>%s</fg>", $line['status']),
          'descr'   => $line['desc'],
        ));
      }

      $table->draw();
    }
  }

  private function filterBranch($repository_api) {
    $branches = $repository_api->getAllBranches();
    $hotfixBranches = array();
    foreach ($branches as $branch) {
      if(strpos($branch['name'],"hotfix") === 0) {
        $hotfixBranches[] = $branch;
      }
    }
    if (count($hotfixBranches) === 0) {
      throw new ArcanistUsageException(
        pht('No branches in this working copy.'));
    }

    $hotfixBranches = $this->loadCommitInfo($hotfixBranches);
    $revisions = $this->loadRevisions($hotfixBranches);
    $this->printBranches($hotfixBranches, $revisions);
  }
}
