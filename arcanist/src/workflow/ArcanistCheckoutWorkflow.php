<?php

/**
 * Switch branches or restore working tree files.
 *
 * @concrete-extensible
 */
class ArcanistCheckoutWorkflow extends ArcanistWorkflow
{

  private $branch;

  public function getWorkflowName()
  {
    return 'checkout';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **checkout** __branch_name__
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git checkout'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
        '*' => 'branch'
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $names = $this->getArgument('branch');

    if ($names && count($names) == 1) {
        $branchName = $names[0];
        $exec = $repository_api->execManualLocal('checkout %s', $branchName);
        list($err, $stdout, $stderr) = $exec;
        echo $stdout;
        fprintf(STDERR, '%s', $stderr);
        return $err;
    }
    else 
        throw new ArcanistUsageException(pht('Wrong syntax.'));
  }

}

