<?php

/**
 * Join two or more development histories together.
 *
 * @concrete-extensible
 */
class ArcanistMergeWorkflow extends ArcanistWorkflow
{

  private $branch;

  public function getWorkflowName()
  {
    return 'merge';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **merge** __branch_name__
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git merge'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
        '*' => 'branch'
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $names = $this->getArgument('branch');

    if (count($names) > 1)
        throw new ArcanistUsageException(pht('Wrong syntax.'));

    if (!$names) {
        $exec = $repository_api->execManualLocal('merge');
        list($err, $stdout, $stderr) = $exec;
    } else {
        $branchName = $names[0];
        $exec = $repository_api->execManualLocal('merge %s', $branchName);
        list($err, $stdout, $stderr) = $exec;
    }

    fprintf(STDERR, '%s', $stderr);
    echo $stdout;
    return $err;
  }

}

