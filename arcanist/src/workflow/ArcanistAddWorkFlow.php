<?php

/**
 * Add file contents to the index.
 *
 * @concrete-extensible
 */
class ArcanistAddWorkFlow extends ArcanistWorkflow
{

  private $add;

  public function getWorkflowName()
  {
    return 'add';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **status** [__options__] [--] [pathspec]
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git status'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
      '*' => 'add',
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $argument = $this->getArgument('add');
    if(count($argument) > 0) {
      $argument = implode(' ', $argument);
      $repository_api = $this->getRepositoryAPI();
      $exec = $repository_api->execManualLocal(
        'add %s', $argument);
      list($err, $stdout, $stderr) = $exec;
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else {
      throw new ArcanistUsageException(pht('Syntax error.'));
    }
  }

}

