<?php

/**
 * Show commit logs.
 *
 * @concrete-extensible
 */
class ArcanistLogWorkflow extends ArcanistWorkflow
{

  private $log;

  public function getWorkflowName()
  {
    return 'log';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **log**
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git log'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array();
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $exec = $repository_api->execManualLocal(
      'log --reverse');
    list($err, $stdout, $stderr) = $exec;
    echo $stdout;
    fprintf(STDERR, '%s', $stderr);
    return $err;
  }

}

